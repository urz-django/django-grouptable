#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='django-grouptable',
    version='0.1',
    description="Simple app for Django's built-in admin which can be used to visualize and manage group membership.",
    url='https://gitlab.hrz.tu-chemnitz.de/urz-django/django-grouptable/',
    packages=find_packages(exclude=["contrib", "tests"]),
    package_data={
        'grouptable': [
            'static/grouptable/*.css',
            'templates/admin/grouptable/*.html'
        ]
    },
    install_requires=[
        'django>=2.2',
    ],
)
