from django.apps import AppConfig


class GrouptableConfig(AppConfig):
    name = 'grouptable'
