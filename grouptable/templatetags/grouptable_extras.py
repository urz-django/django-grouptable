from django import template
from django.contrib.auth.models import Group, User
from django.utils.html import format_html

register = template.Library()


@register.simple_tag
def user_group_checkbox(user_group_dict: dict, user: User, group: Group, name: str):
    return format_html(
        '<input type="checkbox" name="{name}" value="{value}" {checked}>',
        name=name,
        value=f'{user.id}|{group.id}',
        checked='checked' if user_group_dict[(user.id, group.id)] else '',
    )

@register.simple_tag
def permissions_for_group_tooltip(permissions_by_group_dict : dict, group_name: str):
    """
    Renders a help tooltip for a group, which contains the permission of the group.

    .. attention::
        Needs `grouptable.css`.
    """
    permissions = permissions_by_group_dict[group_name]
    return format_html(
        '<span class="grouptable-permission-hint" title="{}"></span>',
        ",\n".join([str(i) for i in permissions])
    )
