from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import transaction
from django.http.request import HttpRequest
from django.http.response import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404

from .models import GroupProxy

User = get_user_model()


class UserGroupAdmin(admin.ModelAdmin):
    class Media:
        css = {
            "all": ("grouptable/grouptable.css", )
        }

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.has_perms(['change_user', 'change_group'])

    def has_delete_permission(self, request, obj=None):
        return False

    @transaction.atomic
    def changelist_view(self, request: HttpRequest, extra_context=None):
        extra_context = extra_context or {}
        user_list = User.objects.filter(is_staff=True).order_by('username')
        group_list = Group.objects.all().order_by('name')
        data = request.POST or None
        checkbox_name = 'usergroups'
        usergroups = data.getlist(checkbox_name) if data else None
        textinput_name = 'username'
        new_staff_name = data.get(textinput_name) if data else None

        if request.method == 'POST':
            user_group_list = [[int(i) for i in ug.split('|')] for ug in usergroups]
            key_set = set(ug[1] for ug in user_group_list)
            user_group_dict = dict((key, []) for key in key_set)
            for u, g in user_group_list:
                user_group_dict[g].append(u)
            for group_id, user_id_list in user_group_dict.items():
                group = get_object_or_404(Group, id=group_id)
                users = User.objects.filter(id__in=user_id_list)
                group.user_set.set(users)

            if new_staff_name:
                User.objects.filter(username=new_staff_name).update(is_staff=True)

            return HttpResponseRedirect(reverse('admin:grouptable_groupproxy_changelist'))

        user_group_dict = dict()
        permissions_by_group_dict = {}
        for group in group_list:
            group_members = group.user_set.all()
            for user in user_list:
                user_group_dict[(user.id, group.id)] = user in group_members
            permissions_by_group_dict[group.name] = group.permissions.all().order_by(
                "content_type__app_label", "content_type__model", "name"
            )

        extra_context.update(
            user_group_dict=user_group_dict,
            group_list=group_list,
            user_list=user_list,
            name=checkbox_name,
            input_name=textinput_name,
            permissions_by_group_dict=permissions_by_group_dict,
        )
        return super().changelist_view(request, extra_context=extra_context)


admin.site.register(GroupProxy, UserGroupAdmin)
