from django.contrib.auth.models import Group
from django.db import models


class GroupProxy(Group):
    class Meta:
        proxy = True
        verbose_name = 'Benutzer-Gruppen-Übersicht'
        verbose_name_plural = 'Benutzer-Gruppen-Übersicht'
